#ifndef _CSCI441_WORLD_H_
#define _CSCI441_WORLD_H_

#include <assert.h>

#include "shape.h"

class World {
private:
    bool _locked;
    std::vector<const Shape*> _shapes;

        
    

public:

    World() : _locked(false) {}

    bool is_locked() const {
        return _locked;
    }

    void append(const Sphere& s) {
        assert(!is_locked());
        _shapes.push_back(&s);
    }

    void append(const Triangle& t) {
//	    std::cout << "Pushing on a triangle" << t.reflective << " length " << _shapes.size() << std::endl;
        assert(!is_locked());
	_shapes.push_back(&t);
	/*
	_triangles[_triangles.size()-1].reflective = t.reflective;
	_triangles[_triangles.size()-1].refractive = t.refractive;
	_triangles[_triangles.size()-1].n = t.n;
	*/

//	std::cout << "Reflective coeff of tri" << _shapes[_shapes.size()-1]->reflective << _shapes.size() << std::endl;

	
    }
    void lock() {
        assert(!is_locked());

        _locked = true;
   }

    const std::vector<const Shape*>& shapes() const {
        assert(is_locked());
        return _shapes;
    }
};

#endif
