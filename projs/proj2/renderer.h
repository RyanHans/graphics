#ifndef _CSCI441_RENDERER_H_
#define _CSCI441_RENDERER_H_

#include "camera.h"
#include "hit.h"
#include "light.h"
#include "ray.h"
#include "intersector.h"

class Renderer {

    Intersector* _intersector;

    int clamp255(float v) {
        return std::max(0.0f, std::min(255.0f, 255.0f*v));
    }


    rgb_t to_color(const glm::vec3 c) {
        return make_colour(clamp255(c.x), clamp255(c.y), clamp255(c.z));
    }


    glm::vec3 phong(const Hit& hit, const Light* light, const glm::vec3& eye) {

        float ambientStrength = 0.2;
        float specularStrength = 0.5;
        float shinyness = 2;

        glm::vec3 pos = hit.position();
        glm::vec3 normal = hit.normal();

        glm::vec3 light_dir = glm::normalize(light->direction(pos));

        float ambient = ambientStrength;

        float diffuse = glm::max(glm::dot(normal, light_dir), 0.0f);

        glm::vec3 view_dir = glm::normalize(eye - pos);
        glm::vec3 reflectDir = glm::reflect(-light_dir, normal);
        float specular = specularStrength *
            pow(std::fmax(glm::dot(view_dir, reflectDir), 0.0), shinyness);

        glm::vec3 light_color  =
            (ambient+diffuse+specular)
            * light->attenuation(pos)
            * light->color();

        return light_color*hit.color();
    }


    glm::vec3 shade(const Camera& camera, const Lights& lights, const Hit& hit, int depth) {
	
        glm::vec3 color = glm::vec3(0,0,0);
	if(!hit.is_intersection() && depth == 0){
		color += camera.background;
	}

        if (hit.is_intersection()) {
            color = glm::vec3(0,0,0);
            for (auto light : lights) {
                color += phong(hit, light, camera.pos);
            }
        }
        return color;
    }

    glm::vec3 render_pixel(
        const Camera& camera,
        const Lights& lights,
        const World& world,
        const Ray& ray,
	const int depth
    ) {
	int cutoff_depth=1;
	//Light intensity decrease on boundary action
	float atten = 1;

        Hit hit = _intersector->find_first_intersection(world, ray);
	glm::vec3 regular_color(0,0,0); 	if(depth == 0){
	}

	glm::vec3 reflected_color(0,0,0);
	glm::vec3 refracted_color(0,0,0);

	regular_color = regular_color + shade(camera, lights, hit, depth);


	//Make sure no infinite bounces

	
	if(depth < cutoff_depth && hit.is_intersection()){
		
		//std::cout << "Hit is an intersection!" << std::endl;


		glm::vec3 newpos = hit.position();
		glm::vec3 normal = hit.normal();
		
		//Handle reflections
		//std::cout << "Hit color" << hit.color().x << std::endl;

		if(hit.isreflective()){


			//std::cout << "Doing reflections" << hit.color().x << std::endl;
			
			Ray reflected = ray.reflected(ray, normal, newpos);

			//Index of refraction same on reflection
			reflected.n = ray.n;
			glm::vec3 addition = atten*(render_pixel(camera, lights, world, reflected, (depth+1)));


			reflected_color = reflected_color + addition;
		}
	
		
		//Handle refractions
		
		if(hit.isrefractive()){


//	std::cout << "Before " << regular_color.z << std::endl;
			//std::cout << "Doing refractions" << std::endl;
			std::cout << "Prev ray" << ray.direction.z << std::endl;
			Ray refracted = ray.refracted(ray, normal, newpos, ray.n, hit.n());
			std::cout << "New ray" << refracted.direction.z << std::endl;
			//Set current index of refraction to shapes
			
			refracted.n = hit.n();

			refracted_color = refracted_color + atten*(render_pixel(camera, lights, world, refracted, (depth+1)));

//	std::cout << "After - " << regular_color.z << std::endl;
		}
		
			
	}
	
    	
	glm::vec3 result = regular_color + refracted_color + reflected_color;



        return result;
    }

public:

    Renderer(Intersector* intersector) : _intersector(intersector) { }

    void render(
        bitmap_image &image,
        const Camera& camera,
        const Lights& lights,
        const World& world
    ) {
	    //For each pixel
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                Ray ray = camera.make_ray(image.width(), image.height(), x, y);
                glm::vec3 c = render_pixel(camera, lights, world, ray, 0);
                image.set_pixel(x, image.height()-y-1, to_color(c));
            }
        }
    }
};

#endif
