#ifndef _CSCI441_RAY_H_
#define _CSCI441_RAY_H_

#include <glm/glm.hpp>
#include <math.h>

struct Ray {
    glm::vec3 origin;
    glm::vec3 direction;
    float n = 1;

    glm::vec3 eval(float t) const {
        return origin + t*direction;
    }

    //Ray reflection
    Ray reflected(Ray init, glm::vec3 normal, glm::vec3 point) const{
	    
	    /*
	    float scale = 2*(glm::dot(normal, init.direction));
	    glm::vec3 newdir = (scale*normal) - init.direction;
	    */

	    glm::vec3 ref = glm::reflect(init.direction, normal);

	    Ray reflect;
	    reflect.origin = point;
	    reflect.direction = ref;
	    


	    return reflect;
    }

    
    //Ray refraction
    Ray refracted(Ray init, glm::vec3 normal, glm::vec3 point, float n1, float n2) const{
	    //Vector form of snell's law
	    float scale = n1/n2;
	    /*
	    glm::vec3 term1 = scale*glm::cross(normal, glm::cross(-normal,init.direction));
	    float sqscale = (scale*scale);
	    glm::vec3 crossed = glm::cross(normal,init.direction);
	    glm::vec3 inside = sqscale*glm::dot(crossed,crossed);
	    //std::cout << inside << std::endl;

	    //glm::vec3 term2 = normal*(sqrt(1-inside));
	    */
	    
	    //I found an easier way built in
	    glm::vec3 direct = glm::refract(init.direction, normal, scale);
	    Ray refract;
	    refract.origin = point;
	    //refract.direction = term1 - term2;
	    refract.direction = direct;
	    return refract;
    }
    

	    

};

#endif
