#version 330 core
in vec3 ourColor;
in vec3 outPos;
in vec3 outnorm;

uniform vec3 lightPos;
uniform mat4 model;
uniform mat4 camera;
out vec4 fragColor;
vec3 realpos;
float color;
vec3 dir;
float phong;


void main() {
	realpos = normalize((camera *model* vec4(lightPos, 1.0)).xyz);
dir = normalize(realpos - outPos);
color = dot(dir, outnorm);
color = max(0, color);
vec3 reflected = normalize((2*(dot(outnorm,dir)*outnorm))-dir);
phong = pow(max(dot(vec3(0, 0, -2)-outPos, reflected),0), 3);
float netcolor = clamp((.1 + color + phong), 0, 1);

    fragColor = vec4(netcolor*ourColor, 1.0f);
}
