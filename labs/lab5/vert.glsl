#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 norm;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;
uniform mat3 invtrans;

out vec3 ourColor;
out vec3 outPos;
out vec3 outnorm;

void main() {
    gl_Position = projection * camera * model * vec4(aPos, 1.0);
	outPos = (model * vec4(aPos, 1.0)).xyz;
	outnorm = invtrans * norm;
    ourColor = aColor;
}
