#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/transforms.h>

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int processInput(GLFWwindow *window, int s) {
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
	    return s+1;


    }
    else{ return s; }
}

int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };
    
    //Convert input triangle to point data type
/*
    Point list[6];
    for(int i=0; i<6; i++){
	    list[i] = Point(triangle[5*i], triangle[5*i+1],
			    triangle[5*i+2],triangle[5*i+3],
				triangle[5*i+4]);
	    std::cout << list[i] << std::endl;
    }
    */

      /* 
    //Scale r(.5);
    //Rotate r(.8);
    //Translate r(.1, .2);
    //Transform points
    for(int i=0; i < 6; i++){
	    list[i] = r*list[i];
    }
    


    

    //Convert back to triangles
    for(int i =0; i < 6; i++){
	    triangle[5*i] = list[i].x;
	    triangle[5*i+1] = list[i].y;
	    triangle[5*i+2] = list[i].r;
	    triangle[5*i+3] = list[i].g;
	    triangle[5*i+4] = list[i].b;
    } 
    */

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    
    glEnableVertexAttribArray(0);

    
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    int count = 0;
    int counter = 0;
    int modes = 7;
    float identity[9] = {1,0,0,0,1,0,0,0,1};
    Matrix3 s(identity);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);
	count = processInput(window, count);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);



    


        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */

//MY CODE
//I need to put the transformation matrix on the shader and use
//the shader to do math???

	float timeValue = glfwGetTime();
	float mod = sin(timeValue) / 2.0f + .5f;
	float balance = sin(timeValue);
    float g = sin(timeValue) / 2.0f +.5f;
    float b = sin(3*timeValue) / 2.0f + .5f;
    float r = sin(4*timeValue) / 2.0f + .5f;
    float o = sin(timeValue/2) / 2.0f + .5f;


    int vec2[2];

    if(count <= 10){
	    Matrix3 r(identity);

	    s = r;
    }else
    if(count <= 20){
		Translate r(balance/100, balance/100);
	    s = r*s;
    }else
    if(count <= 30){
	    Rotate r(.01);
	    s = r*s;
    }else
    if(count <= 40){
		Scale r(mod, mod);

	    s = r;
    }else
    if(count <= 50){

	    Scale r(5*mod);
	    s = r;
    }else
    if(count <= 60){
	    if(counter == 0){
		    Translate t2(.25,-.25);
		    s=t2;
		    counter = counter+1;
	    }else{
	    Translate r1(-.25,.25);
	    Rotate r2(.01);
	    Translate r3(.25, -.25);
	    s=r3*r2*r1*s;
	    }
    }else
    if(count <= 70){
	    count = 0;
	    counter = 0;
    }
    




    int vertexcolor = glGetUniformLocation(shader.id(), "myColor");
    int first = glGetUniformLocation(shader.id(), "first");
	int sec = glGetUniformLocation(shader.id(), "sec");
	int third = glGetUniformLocation(shader.id(), "third");

	//std::cout << s.matrix[0] << std::endl;



    glUniform4f(vertexcolor, r, g, b, o);
    glUniform3f(first, s.matrix[0], s.matrix[1], s.matrix[2]);
	glUniform3f(sec, s.matrix[3], s.matrix[4], s.matrix[5]);
	glUniform3f(third, s.matrix[6], s.matrix[7], s.matrix[8]);




    
    

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
