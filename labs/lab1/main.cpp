#include <iostream>

#include "bitmap_image.hpp"

//Point class for storing point data
class Point {
    public:
	    float x;
	    float y;
	    float r;
	    float g;
	    float b;
    //Constructor
    Point(float xx, float yy, float rr, float gg, float bb) : x(xx), y(yy), r(rr), g(gg), b(bb) {
	    //nothing to do
	    //std::cout << "In point constructor" << std::endl;
    }

    //default
    Point() : x(0), y(0), r(1), g(1), b(1) {
    }
    ~Point(){
	    //std::cout << "Destructing point" <<std::endl;
    }
};

std::ostream& operator<<(std::ostream& stream, const Point& p){
    stream << "<" << p.x << "," << p.y << ">";
    return stream;
}

//method that calculates max+min values for a bounding box
void boundingbox(Point set[3], float *minx, float *miny, float *maxx, float *maxy) {

	*maxx = set[0].x;
	*maxy = set[0].y;
	*minx = set[0].x;
	*miny = set[0].y;

	for(int i = 1; i<3; i++){
		//std::cout << set[i].x << set[i].y << std::endl;
		if(set[i].x > *maxx){
	
			*maxx = set[i].x;
		}
		if(set[i].x < *minx){
			*minx = set[i].x;
		}
		if(set[i].y > *maxy){
			*maxy = set[i].y;
		}
		if(set[i].y < *miny){
			*miny = set[i].y;
		}

	}
	//return minx, maxx, miny, maxy;
}
		

int main(int argc, char** argv) {
    /*
      Prompt user for 3 points separated by whitespace.

      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

    // create an image 640 pixels wide by 480 pixels tall
        
    bitmap_image image(640, 480);
    
    //User Input
    char drop;
    float xi, yi, ri, gi, bi;
    int size = 3;
    Point array[size];

    std::cout << "Enter 3 points (enter a point as x,y:r,g,b):" << std::endl;
    for(int i = 0; i < size; i++){
	    std::cin >> xi >> drop >> yi >> drop >> ri >> drop >> gi >> drop >> bi;
	    array[i] = Point(xi,yi,ri,gi,bi);

    }

    /*Testing Portion to negate need for user input
	Point al(50,50,1,0,0);
    Point be(600,20,0,1,0);
    Point ga(300,400,0,0,1);
    Point array[3] = {al, be, ga};

    */


    //Find Specs for Bounding Box
    float minx, maxx, miny, maxy;
    boundingbox(array, &minx, &miny, &maxx, &maxy);
    //std::cout << minx << "," << maxx << ":" << miny << "," << maxy << std::endl;



    //Barycentric coords correspondence
    Point alpha = array[0];//(0,0)
    Point beta = array[1];//(1,0)
    Point gamma = array[2];//(0,1)
    float determinant = ((beta.y-gamma.y)*(alpha.x-gamma.x)+(gamma.x-beta.x)*(alpha.y-gamma.y));
    
    float acoeff, bcoeff, gcoeff;
    float a1 = (beta.y-gamma.y);
    float a2 = (gamma.x-beta.x);
    float b1 = (gamma.y-alpha.y);
    float b2 = (alpha.x-gamma.x);
    
    //Color variables that I will be using later to interpolate
    float r, g, b;
    int red, green, blue;
    
    //loop over every point in bounding box
    for(int y = miny; y <maxy; y++){
	    for(int x = minx; x < maxx; x++){
		    //Barycentric Coords

		    
		    acoeff = (a1*(x-gamma.x)+a2*(y-gamma.y))/determinant;
		    bcoeff = (b1*(x-gamma.x)+b2*(y-gamma.y))/determinant;
		    gcoeff = 1-acoeff-bcoeff;
		    
		    if(acoeff <= 1 && bcoeff <= 1 && gcoeff <=1 && acoeff >=0 && bcoeff >=0 && gcoeff >=0){
			    //Color decision
			    
			    r = acoeff*alpha.r+bcoeff*beta.r+gcoeff*gamma.r;
			    g = acoeff*alpha.g+bcoeff*beta.g+gcoeff*gamma.g;
			    b =  acoeff*alpha.b+bcoeff*beta.b+gcoeff*gamma.b;

			    
			    red = (int) 255*r;
			    green = (int) 255*g;
			    blue = (int) 255*b;



			    rgb_t color = make_colour(red, green, blue);
			    //rgb_t color = make_colour(255,255,255);
			    image.set_pixel(x,y,color);
		    }
	    }
    }
		    



    



    /*
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          rgb_t color = make_color(255, 255, 255);
          image.set_pixel(x,y,color);

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
}
