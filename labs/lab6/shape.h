#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>
#include <temp/csci441/vector3.h>


			


std::vector<float> generate_norms(std::vector<float> coords, int x){
	std::vector<float> shape;
	//iterates over the faces
	for(int i = 0; i < coords.size()/27; i++){

		Vector3 a(coords[27*i+9]-coords[27*i], coords[27*i+10]-coords[27*i+1], coords[27*i+11]-coords[27*i+2]);

		Vector3 b(coords[27*i+18]-coords[27*i], coords[27*i+19]-coords[27*i+1], coords[27*i+20]-coords[27*i+2]);
		Vector3 n = norm(a^b);
		
		
		if(x == 0){
n = -n;
		}
		if(x == 1){

	if(i%2 == 1){
		n = -n;
	}
		}
		

		

		//vert 1
		shape.push_back(coords[27*i]);
		shape.push_back(coords[27*i+1]);
		shape.push_back(coords[27*i+2]);
		shape.push_back(coords[27*i+3]);
		shape.push_back(coords[27*i+4]);
		shape.push_back(coords[27*i+5]);
		//Normal
		
		shape.push_back(n.x);
		shape.push_back(n.y);
		shape.push_back(n.z);


		//Vert 2
		shape.push_back(coords[27*i+9]);
		shape.push_back(coords[27*i+10]);
		shape.push_back(coords[27*i+11]);
		shape.push_back(coords[27*i+12]);
		shape.push_back(coords[27*i+13]);
		shape.push_back(coords[27*i+14]);
		//Normal
		shape.push_back(n.x);
		shape.push_back(n.y);
		shape.push_back(n.z);

		//Vert 3
		shape.push_back(coords[27*i+18]);
		shape.push_back(coords[27*i+19]);
		shape.push_back(coords[27*i+20]);
		shape.push_back(coords[27*i+21]);
		shape.push_back(coords[27*i+22]);
		shape.push_back(coords[27*i+23]);
		//Normal
		shape.push_back(n.x);
		shape.push_back(n.y);
		shape.push_back(n.z);
	}

	return shape;

}
std::vector<float> smooth_norms(std::vector<float> coords){

		//Find faces the vertex is a part of
	for(int i = 0; i < coords.size()/9; i++){
		Vector3 normal(0,0,0);
		int count = 0;
		std::vector<float> indexes;



		float x = coords[9*i];  
		float y = coords[9*i+1];
		float z = coords[9*i+2];
		//Vector3 initnormal(coords[9*i+6],coords[9*i+7],coords[9*i+8]);
		//normal = normal + initnormal;
		count++;
		for( int j = 0; j < coords.size()/9; j++){
			float x2 = coords[9*j];  
			float y2 = coords[9*j+1];
			float z2 = coords[9*j+2];
			if(x==x2 && y==y2 && z==z2){
				Vector3 add(coords[9*j+6],coords[9*j+7],coords[9*i+8]);

				normal = normal + add;
				count++;
				indexes.push_back(9*j);
			}
		}
		Vector3 result = norm(normal);
		for(int p = 0; p < indexes.size(); p++){

			coords[indexes[p]+6] = result.x;  
			coords[indexes[p]+7] = result.y;
			coords[indexes[p]+8] = result.z;
		}
	}
	return coords;
}






template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y, const N& z,
        const C& r, const C& g, const C& b,
        const Vector4& n=Vector4(1,0,0), bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(z);
    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);

    Vector4 normal = n.normalized();
    coords.push_back(normal.x());
    coords.push_back(normal.y());
    coords.push_back(normal.z());
}

class DiscoCube {
public:
    std::vector<float> coords;
    DiscoCube() : coords{
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,

        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,

        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0
    } {}

};

template <class T>
class List {
	public:
		T root;
		
	List(T first) : root(first){
	}
	void add (T n);
	
};
template <class T>
void List<T>::add(T n){
		T iter = root;
		while(iter.next != NULL){
			iter = *iter.next;
		}
		iter.next= &n;
	}

class Vertex {
	public:
		float x, y, z, r, g, b, nx, ny, nz;
		Vertex *next;
	Vertex(float ix, float iy, float iz, float ir, float ig, float ib, float inx, float iny, float inz){
		x = ix;
		y = iy;
		z = iz;
		r = ir;
		g = ig;
		b = ib;
		nx = inx;
		ny = iny;
		nz = inz;
	}
	void setNext(Vertex n){
		next = &n;
	}
	/*
	bool operator== (Vertex a, Vertex b){
		bool res = (a.x == b.x && a.y == b.y && a.z == b.z);
		return res;
	}
	*/

		
};


class Face {
	public:
		Vertex a, b, c;
		Face *next;
	Face(Vertex ia, Vertex ib, Vertex ic) : a(ia), b(ib), c(ic){

	}
	void setNext(Face n){
		next = &n;
	}
	/*
	bool operator ==(Face x, Face y){
		bool res = (x.a == y.a && x.b == y.b && x.c == y.c);
		return res;
	}
	*/
};


class Cylinder {
public:
    std::vector<float> coords;
    Cylinder(unsigned int n, float r, float g, float b) {
        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);

            // add triangle vip1L, viH, vip1H
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, vip1_x, h, vip1_y, r, g, b);

            // add high triangle vi, vip1, 0
            Vector4 nh(0, 1, 0);
            add_vertex(coords, vip1_x, h, vip1_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, c_x, h, c_y, r, g, b);

            // // add low triangle vi, vip1, 0
            Vector4 nl(0, -1, 0);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, c_x, -h, c_y, r, g, b);
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
        }
    }
};


class Cone {
public:
    std::vector<float> coords;
    Cone(unsigned int n, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            // add triangle viL, viH, vip1L
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
            add_vertex(coords, c_x, h, c_y, r, g, b);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);

            // // add low triangle vi, vip1, 0
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, c_x, -h, c_y, r, g, b);
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
        }
    }
};

class Sphere {
    double x(float r, float phi, float theta){
        return r*cos(theta)*sin(phi);
    }

    double y(float r, float phi, float theta){
        return r*sin(theta)*sin(phi);
    }

    double z(float r, float phi, float theta){
        return r*cos(phi);
    }

public:
    std::vector<float> coords;
    Sphere(unsigned int n, float radius, float r, float g, float b) {
        int n_steps = (n%2==0) ? n : n+1;
        double step_size = 2*M_PI / n_steps;

        for (int i = 0; i < n_steps/2.0; ++i) {
            for (int j = 0; j < n_steps; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n_steps)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n_steps)*step_size;

                // vertex i,j
                double vij_x = x(radius, phi_i, theta_j);
                double vij_y = y(radius, phi_i, theta_j);
                double vij_z = z(radius, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(radius, phi_ip1, theta_j);
                double vip1j_y = y(radius, phi_ip1, theta_j);
                double vip1j_z = z(radius, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(radius, phi_i, theta_jp1);
                double vijp1_y = y(radius, phi_i, theta_jp1);
                double vijp1_z = z(radius, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(radius, phi_ip1, theta_jp1);
                double vip1jp1_y = y(radius, phi_ip1, theta_jp1);
                double vip1jp1_z = z(radius, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }
};
/*
class Torus {
    double x(float c, float a, float phi, float theta) {
        return (c+a*cos(theta))*cos(phi);
    }

    double y(float c, float a, float phi, float theta) {
        return (c+a*cos(theta))*sin(phi);
    }

    double z(float c, float a, float phi, float theta) {
        return a*sin(theta);
    }

public:
    std::vector<float> coords;
    Torus(unsigned int n, float c, float a, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n)*step_size;

                // vertex i,j
                double vij_x = x(c, a, phi_i, theta_j);
                double vij_y = y(c, a, phi_i, theta_j);
                double vij_z = z(c, a, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(c, a, phi_ip1, theta_j);
                double vip1j_y = y(c, a, phi_ip1, theta_j);
                double vip1j_z = z(c, a, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(c, a, phi_i, theta_jp1);
                double vijp1_y = y(c, a, phi_i, theta_jp1);
                double vijp1_z = z(c, a, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(c, a, phi_ip1, theta_jp1);
                double vip1jp1_y = y(c, a, phi_ip1, theta_jp1);
                double vip1jp1_z = z(c, a, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }

};
*/
class Torus {
    double x(float c, float a, float phi, float theta){
        return (c+a*cos(theta))*cos(phi);
    }

    double y(float c, float a, float phi, float theta){
        return (c+a*cos(theta))*sin(phi);
    }

    double z(float c, float a, float phi, float theta){
        return a*sin(theta);
    }

public:
    std::vector<float> coords;
    Torus(unsigned int n, float c, float a, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n)*step_size;

                // vertex i,j
                double vij_x = x(c, a, phi_i, theta_j);
                double vij_y = y(c, a, phi_i, theta_j);
                double vij_z = z(c, a, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(c, a, phi_ip1, theta_j);
                double vip1j_y = y(c, a, phi_ip1, theta_j);
                double vip1j_z = z(c, a, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(c, a, phi_i, theta_jp1);
                double vijp1_y = y(c, a, phi_i, theta_jp1);
                double vijp1_z = z(c, a, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(c, a, phi_ip1, theta_jp1);
                double vip1jp1_y = y(c, a, phi_ip1, theta_jp1);
                double vip1jp1_z = z(c, a, phi_ip1, theta_jp1);

                // add triangle
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

                // add triange
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
            }
        }
    }

};

/*
List <Vertex> vlist(std::vector<float> coords){
	float v[9];
	//init list
	v[0]=coords[0];
	v[1]=coords[1];
	v[2]=coords[2];
	v[3]=coords[3];
	v[4]=coords[4];
	v[5]=coords[5];
	v[6]=coords[6];
	v[7]=coords[7];
	v[8]=coords[8];
	List <Vertex> verts(Vertex(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8]));


	//loop the rest
	for(int i = 1; i < coords.size()/9; i++){
		v[0]=coords[9*i];
		v[1]=coords[9*i+1];
		v[2]=coords[9*i+2];
		v[3]=coords[9*i+3];
		v[4]=coords[9*i+4];
		v[5]=coords[9*i+5];
		v[6]=coords[9*i+6];
		v[7]=coords[9*i+7];
		v[8]=coords[9*i+8];
		verts.add(Vertex(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8]));

	}
	return verts;
}

List <Face> flist(List <Vertex> v){
	Vertex f[3];
	//init list
	v[0]=coords[0];
	v[1]=coords[1];
	v[2]=coords[2];
		List <Vertex> verts(Vertex(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8]));


	//loop the rest
	for(int i = 1; i < coords.size()/9; i++){
		v[0]=coords[9*i];
		v[1]=coords[9*i+1];
		v[2]=coords[9*i+2];
		v[3]=coords[9*i+3];
		v[4]=coords[9*i+4];
		v[5]=coords[9*i+5];
		v[6]=coords[9*i+6];
		v[7]=coords[9*i+7];
		v[8]=coords[9*i+8];
		verts.add(Vertex(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8]));

	}
	return verts;
}

*/

#endif
