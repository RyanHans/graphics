#include <iostream>

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>
#include <csci441/vector4.h>

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Ray {
	glm::vec3 origin;
	glm::vec3 dir;

	Ray(const glm::vec3& iorigin=glm::vec3(0,0,0), const glm::vec3& idir = glm::vec3(1,0,0)){
		origin = iorigin;
		dir = idir;

	}
};
	

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(glm::vec3 icenter,
            float iradius,
            glm::vec3 icolor)
        {
		center = icenter;
		color = icolor;
		radius = iradius;
            static int id_seed = 0;
            id = ++id_seed;
        }
    Sphere(){
	    center = glm::vec3(0,0,0);
	    color = glm::vec3(50,50,50);
	    radius = 50;
    }
};

struct Package {
	Sphere s;
	float shortest;
	Package(Sphere is, float ishortest){
		s = is;
		shortest = ishortest;
	}
};

float intersection(Ray r, Sphere s){
	glm::vec3 diff = s.center - r.origin;

	float scale = (diff.x*r.dir.x) + (diff.y*r.dir.y) + (diff.z*r.dir.z);
	glm::vec3 proj = scale*r.dir;

        float height = length(s.center - (proj+r.origin));
	
		
	//std::cout << "Height: " << height << std::endl;

	if(height > s.radius){
		return 10000;

	}else{
		/*
std::cout << "Height: " << height << std::endl;
	std::cout << "Radius: " << s.radius << std::endl;
	*/

		float offset = sqrt((s.radius*s.radius)-(height*height));
		glm::vec3 change = offset*r.dir;
		return std::min(abs(length(proj+change)),abs(length(proj-change)));
	}
				

	
}




void render(bitmap_image& image, const std::vector<Sphere>& world) {
    // TODO: implement ray tracer
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    int width = 640;
    int height = 480;
    bitmap_image image(width, height);

    

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(300, 400, 600), 50, glm::vec3(1,1,0)),
        Sphere(glm::vec3(200, 300, 400), 75, glm::vec3(0,1,1)),
        Sphere(glm::vec3(50, 350, 100), 100, glm::vec3(1,0,1)),
    };




	


    for(int i = 0; i < width; i++){

	    for(int j = 0; j < height; j++){
		std::vector<Package> candidates = {};

		    rgb_t color = make_colour(50,150,200);
		    //image.set_pixel(i,j,color);
		    //generate ray origin
		    /*
		    //ortho
		    glm::vec3 coords(i,j,1000);
		    //make rays
		std::vector<Ray> gun = {
			Ray(coords, glm::vec3(0,0,-1)),
							
		};
		*/
		    //Perspective
		    glm::vec3 coords(300, 300, 1000);
		    glm::vec3 target(i,j,0);
		    glm::vec3 dir = normalize(target - coords);

		    std::vector<Ray> gun = {
			    Ray(coords, dir)
		    };



		    Sphere first;
		    int intersected = 0;


		    //for each object
		    for(int k = 0; k < world.size(); k++){

			    //for each ray
			    int intersects = 1;
			    float closeness;
			    for(int l = 0; l < gun.size(); l++){

				    //check if all the rays intersect object

				    closeness = intersection(gun[l], world[k]);
				    /*
				    std::cout << "Coords: (" << i << "," << j
					    << "," << gun[l].origin.z << 
					    ") with direction (" << gun[l].dir.x
					    << "," << gun[l].dir.y << "," 
					    << gun[l].dir.z << 
					    "Intersects sphere centered at" <<
					    world[k].center.x << "," <<
					    world[k].center.y << "," <<
					    world[k].center.z << " with radius"
					    << world[k].radius << 
					    " with closeness " << 
					    closeness << std::endl;
					    */
				    if(closeness == 10000){
					    intersects = 0;
				    }

			    }
			    //if they do, make that object a candidate for color
			    if(intersects == 1){
				    candidates.push_back(Package(world[k],closeness));
			    }
		    }

		    float value =10000;
		    for(int m = 0; m < candidates.size(); m++){
		    	if(candidates[m].shortest < value){
				first = candidates[m].s;
				intersected = 1;
				value = candidates[m].s.color.z;
//				std::cout << "Drawing sphere: " << candidates[m].radius << std::endl;
			}
		    }
		

		    //std::cout << med << std::endl;
		    rgb_t color2 = make_colour(255*first.color.x,255*first.color.y,255*first.color.z);
		    if(intersected == 0){
			    image.set_pixel(i,j,color);
		    }else{
			    image.set_pixel(i,j,color2);
		    }




	    }
    }

    // render the world
    render(image, world);

    image.save_image("perspective.bmp");
    std::cout << "Success" << std::endl;
}


