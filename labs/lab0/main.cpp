#include <iostream>
#include <string>
class Vector3 {

	public:
    	float x;
	float y;
    	float z;

	// Constructor
	Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
		// nothing to do here as we've already initialized x, y, and z above
		std::cout << "in Vector3 constructor" << std::endl;
	}

	Vector3() : x(0), y(0), z(0) {//default is <0,0,0>

	}

	// Destructor - called when an object goes out of scope or is destroyed
	~Vector3() {
		//this is where you would release resources such as memory or file descriptors
		// in this case we don't need to do anything
		std::cout << "in Vector3 destructor" << std::endl;
	}

	
		
};

Vector3 add(const Vector3& v, const Vector3& v2) { 
	Vector3 result = Vector3(v.x+v2.x, v.y+v2.y, v.z+v2.z);
	return result;
}

Vector3 operator+(const Vector3& v, const Vector3& v2) { 
	Vector3 result = Vector3(v.x+v2.x, v.y+v2.y, v.z+v2.z);
	return result;
}


Vector3 operator*(Vector3 v, float s) { 
	Vector3 result = Vector3(v.x*s,v.y*s, v.z*s);
	return result;
}


Vector3 operator*(float s, Vector3 v) { 
	Vector3 result = Vector3(v.x*s,v.y*s, v.z*s);
	return result;
}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    // std::cout is a std::ostream object, just like stream
    // so use stream as if it were cout and output the components of
    // the vector
       
	stream << "<" << v.x << "," << v.y << "," << v.z << ">";
	return stream;
}

int main(int argc, char** argv){
	//Part 4
	std::cout << "----Part 4----" << std::endl;
	std::cout << "hello world " << argv[0] << " " << 1234 << " " << std::endl;
	
	std::string name;
	std::cout << "Please enter your name." << std::endl;
	std::cin >> name;
	std::cout << "Hello " << name << std::endl;
	//Part 5
	Vector3 a(1,2,3);
	Vector3 b(4,5,6);
	std::cout << "----Part 5+6----" <<std::endl;
	std::cout << add(a,b) << std::endl;

	std::cout << a+b << std::endl;
	std::cout << "----Part 7----" << std::endl;

	Vector3 v(0,0,0);
	v.y = 5;
	std::cout << "Updated Stack Vector: " << v << std::endl;

	Vector3* heap = new Vector3(0,0,0);
	std::cout << "Heap Vector: " << *heap << std::endl;
	heap->y = 5;
	std::cout << "Updated Heap Vector: " << *heap << std::endl;
	delete heap;

	//Part 8
	std::cout << "----Part 8----" << std::endl;
	int size = 10;
	Vector3 array[size];
	
	Vector3* heapvects = new Vector3[size];

	for(int i = 0; i < size;i++){
		heapvects[i].y = 5;
		std::cout << "Vector " << i << ": " << heapvects[i] << std::endl;
	}
	//Part 9
	//DAS IST EMPTYHAUSEN



	return 0;

}
