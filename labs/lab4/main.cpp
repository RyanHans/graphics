#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/transform4.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    
}
void processInput(GLFWwindow *window, Shader &shader, float* rx, float* ry, float* rz, float* tx, float* ty, float* tz, float* scale, float* pos, int* proj, int* prim) {

if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
	*ty = *ty + .01;
}
if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
	*ty = *ty - .01;
}
if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
	*tx = *tx + .01;
}
if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
	*tx = *tx - .01;
}
if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
	*scale = *scale - .05;
}
if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
	*scale = *scale + .05;
}
if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
	*rx = *rx + .01;
}
if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
	*rx = *rx - .01;
}
if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
	*ry = *ry + .01;
}
if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
	*ry = *ry - .01;
}
if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
	*pos = *pos + .05;
}
if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
	*pos = *pos - .05;
}
if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
	*proj = 1;
}
if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS) {
	*proj = 0;
}
if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS) {
	*prim = 0;
}
if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS) {
	*prim = 1;
}







}






int processInput(GLFWwindow *window, int s) {
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
	    return s+1;


    }

    else{ return s; }
}

GLuint InitObject(float vertices[], int size) {
	// copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);


    return VBO;
}


void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    float vertices2[] = {
	    1, 0, 0, 1, 0, 0,
	    0, 1, 0, 0, 1, 0,
	    0, 0, 1, 0, 0, 1,

	    1, 0, 0, 1, 0, 0,
	    0, 1, 0, 0, 1, 0,
	    0, 0, 0, .5, .5, .5,

	    1, 0, 0, 1, 0, 0,
	    0, 0, 1, 0, 0, 1,
	    0, 0, 0, .5, .5, .5,

	    0, 1, 0, 0, 1, 0,
	    0, 0, 1, 0, 0, 1,
	    0, 0, 0, .5, .5, .5
    };
    int numpoints = 360;
    int bigpoints = numpoints/18;
    float cone[numpoints];
    for(int i = 0; i < bigpoints; i++){
	    int j = 18*i;
	    cone[j]=cos(M_PI*2*i/bigpoints);
	    cone[j+1]=sin(M_PI*2*i/bigpoints);
	    cone[j+2]=0;
	    cone[j+3]=0;
	    cone[j+4]=0;
	    cone[j+5]=1;

	    cone[j+6]=cos(M_PI*2*(i+1)/bigpoints);
	    cone[j+7]=sin(M_PI*2*(i+1)/bigpoints);
	    cone[j+8]=0;
	    cone[j+9]=0;
	    cone[j+10]=0;
	    cone[j+11]=1;

	    cone[j+12]=0;
	    cone[j+13]=0;
	    cone[j+14]=1;
	    cone[j+15]=0;
	    cone[j+16]=0;
	    cone[j+17]=1;
	    

    }

	    


    /* init the model */
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };


    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    //View matrix init
    Vector3 gaze(0, 0, 0);
    Vector3 up(0, 1, 0);
    
    //Key Variables
    float xr = 0;
    float yr = 0;
    float zr = 0;
    float xt = 0;
    float yt = 0;
    float zt = 0;
    float pos = 0;
    float scale = 1;
    int proj= 0;
    int prim = 0;
    Matrix4 p;

    Perspective l(90, 1.5, -30, 30);
    p = l;


    
    /* Loop until the user closes the window */
    float identity[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window, shader);
	processInput(window, shader, &xr, &yr, &zr, &xt, &yt, &zt, &scale, &pos, &proj, &prim);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

	//Transformation Variables
        Vector3 position(0, pos, -1);

	
	
	//Transformations
	Matrix4 s(identity);
	View v(position, gaze, up);
	if(proj == 0){
		Orth z(10, -10, 10, -10, -10, 10);
		p = z;
		
	}else{

		Perspective z(90, 1.5, -10, 10);
		p = z;
	}



	RotateX rx(xr);
	RotateY ry(yr);
	RotateZ rz(zr);
	Translate t(xt, yt, zt);
	Scale sc(scale);
	s = p*v*t*rx*ry*rz*sc;

	
	//Uniforms
	int first = glGetUniformLocation(shader.id(), "first");
	int sec = glGetUniformLocation(shader.id(), "sec");
	int third = glGetUniformLocation(shader.id(), "third");	
	int fourth = glGetUniformLocation(shader.id(), "fourth");


	glUniform4f(first, s.matrix[0], s.matrix[1], s.matrix[2], s.matrix[3]);
	glUniform4f(sec, s.matrix[4], s.matrix[5], s.matrix[6], s.matrix[7]);
	glUniform4f(third, s.matrix[8], s.matrix[9], s.matrix[10], s.matrix[11]);
	glUniform4f(fourth,s.matrix[12], s.matrix[13], s.matrix[14], s.matrix[15]);


        // render the cube
	if(prim == 0){
		GLuint VBO = InitObject(vertices, sizeof(vertices));


		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));
	}else{
	//render triangle
		GLuint VBO = InitObject(cone, sizeof(cone));
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, sizeof(cone));
	}

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
