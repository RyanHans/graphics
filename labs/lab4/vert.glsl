#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;
uniform vec4 first;
uniform vec4 sec;
uniform vec4 third;
uniform vec4 fourth;

void main() {
	vec4 t = vec4(aPos, 1);
vec4 move = vec4(((first.x*t.x)+(first.y*t.y)+(first.z*t.z)+(first.w*t.w)),
((sec.x*t.x)+(sec.y*t.y)+(sec.z*t.z)+(sec.w*t.w)),
((third.x*t.x)+(third.y*t.y)+(third.z*t.z)+(third.w*t.w)),
((fourth.x*t.x)+(fourth.y*t.y)+(fourth.z*t.z)+(fourth.w*t.w)));


    gl_Position = vec4(move.xyz, 1.0);

    ourColor = aColor;
}

